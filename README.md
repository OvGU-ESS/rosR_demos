# rosR_demos

This is a set of sublementary examples for [rosR](https://gitlab.com/OvGU-ESS/rosR.git) language extension.

| | |
| ----------- | ---------------------------------------------------------------------------------------- |
| Authors     | André Dietrich & Sebastian Zug                                                           |
| Source      | `git clone` https://gitlab.com/OvGU-ESS/rosR_demos.git                                   |
| Email       | <mailto:dietrich@ivs.cs.uni-magdeburg.de> & <mailto:zug@ivs.cs.uni-magdeburg.de>         |
| Publication | [The R in Robotics](http://journal.r-project.org/archive/2013-2/dietrich-zug-kaiser.pdf) |
| Talk By     | Jan Wijffels http://www.r-bloggers.com/using-r-in-robotics-applications-with-ros         |